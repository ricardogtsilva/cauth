﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CAuth.Models
{
    public class ProxyMapping
    {
        public string Source { get; set; }
        public string Destiny { get; set; }
    }
}
