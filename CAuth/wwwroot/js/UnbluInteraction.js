﻿/*!***************************************************** UNBLU INTEGRATION PACKAGE *****************************************************
--AUTHOR: IT SECTOR
--PACKAGE NAME: v2019.09.11.626_QA_DB
--PACKAGE DATE: 11 - 09 - 2019
--PACKAGE UUID: 92a44183 - 73fb - 4fda - 9490 - 49fe95cfbaeb


--Powered by IT SECTOR, all rights reserved.

***************************************************** UNBLU INTEGRATION PACKAGE *****************************************************!*/

/******** SETUP PARAMS */
var UNBLUSERVER = "https://example.unblu-test.com";
var APIKEY = "MZsy5sFESYqU7MawXZgR_w";
/******** /SETUP PARAMS */

var triggerObjectID;
var unbluObjectClass = 'x-unblu-launcher-button';

//Unblu API Initialization
function plattformInitialize() {
    if (!unblu.api.isInitialized()) {
        unblu.api.configure({
            serverUrl: UNBLUSERVER,
            apiKey: APIKEY
        }).initialize();
    }
}

//Start new chat session
function startUnbluChat(triggerObject) {

    triggerObjectID = triggerObject;
    $('#' + triggerObjectID).hide();

    var customerName = "";
    startConversation("CHAT", customerName);
}

//Start new CoBrowsing session
function startUnbluCoBrowsing(triggerObject) {

    triggerObjectID = triggerObject;
    $('#' + triggerObjectID).hide();

    var customerName = "";
    startConversation("COBROWSING", customerName);
}


//Unblu conversation handler
function startConversation(conversationType, visitorName) {
    plattformInitialize();

    window.unblu.api.initialize().then(api => {

        $("." + unbluObjectClass).click(function () { closeInteraction(); });
        $('.' + unbluObjectClass).show();

        api.isAgentAvailable().then(agent => {

            switch (conversationType.toUpperCase()) {
                case "CHAT":
                    if (agent) {
                        api.startConversation("CHAT_REQUEST").then(conversation => {
                            conversation.on("end", function () {
                                console.log("Conversation ended.");
                            });
                            conversation.on("close", function () {
                                console.log("Conversation closed.");
                            });
                        }).catch(e => {
                            console.log("error on start conversation: ", e);
                        });

                    } else {
                        api.startConversation("OFFLINE_CHAT_REQUEST").then(conversation => {
                            conversation.on("end", function () {
                                console.log("Conversation ended.");
                            });
                            conversation.on("close", function () {
                                console.log("Conversation closed.");
                            });
                        }).catch(e => {
                            console.log("error on start conversation: ", e);
                        });
                    }
                    break;
                default:
                    api.startConversation("CHAT_REQUEST").then(conversation => {
                        conversation.on("end", function () {
                            console.log("Conversation ended.");
                        });
                        conversation.on("close", function () {
                            console.log("Conversation closed.");
                        });
                    }).catch(e => {
                        console.log("error on start conversation: ", e);
                    });
                    break;
            }
        }).catch(e => {
            console.log("Error on isAgentAvailable: ", e);
        });
    }).catch(e => {
        console.log("plattform initialization error: ", e);
    });

}

//close interaction and hide unblu widget
function closeInteraction() {
    unblu.api.off();
    $('#' + triggerObjectID).show();
    $('.' + unbluObjectClass).hide();
}


function toggleIndividualUi() {
    window.unblu.api.initialize().then(api => {
        api.ui.toggleIndividualUi();
    });
}

function popoutIndividualUi() {
    window.unblu.api.initialize().then(api => {
        api.ui.popoutIndividualUi();
    });
}

function openPinEntry() {
    window.unblu.api.initialize().then(api => {
        api.ui.openPinEntryUi();
    });
}


